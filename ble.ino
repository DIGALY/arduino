#include <SoftwareSerial.h>   // librairie pour creer une nouvelle connexion serie max 9600 baud
#define PIN_1 5
#define PIN_2 6
#define PIN_3 7

#define PIN_lampe 12

 
SoftwareSerial BTSerial(10, 11); // RX | TX  = > BT-TX=10 BT-RX=11
 
void setup()
{
  Serial.begin(9600);
  Serial.println("Enter a command:");
  BTSerial.begin(115200);  // HC-05 9600 baud 
 pinMode(PIN_1, OUTPUT);

 pinMode(PIN_2, OUTPUT);

 pinMode(PIN_3, OUTPUT);

 pinMode(PIN_lampe,OUTPUT);
}
 
void loop()
{
  char message;
  
    // Boucle de lecture sur le serial 
   
    if (BTSerial.available()){
      // Lecture du message envoyé par le serial usb

      delay(10);
      char c = (char) BTSerial.read();
      message=c;
      // Ecriture du message dans le BT
     
      
       //digitalWrite(PIN_LED,HIGH); // led on
        Serial.println(message);
    }
 

    if(message == 's'){// stop
      Serial.println("stop");
      digitalWrite(PIN_1,LOW);
      digitalWrite(PIN_2,LOW);
      digitalWrite(PIN_3,LOW);
    } else if(message == 'm'){// avant
      Serial.println("avance");


      digitalWrite(PIN_1,HIGH);
      digitalWrite(PIN_2,LOW);
      digitalWrite(PIN_3,LOW);

    }else if(message == 'r'){// arrière
      Serial.println("arriere");
      
      digitalWrite(PIN_1,LOW);
      digitalWrite(PIN_2,HIGH);
      digitalWrite(PIN_3,LOW);
}else if(message == 'd'){// droite
      Serial.println("droite");
      
      digitalWrite(PIN_1,HIGH);
      digitalWrite(PIN_2,HIGH);
      digitalWrite(PIN_3,LOW);
}else if(message == 'g'){// gauche
      Serial.println("gauche");
      
      digitalWrite(PIN_1,LOW);
      digitalWrite(PIN_2,LOW);
      digitalWrite(PIN_3,HIGH);
}else if(message == 'l'){// lampe
      Serial.println("lampe_allume");
     
      
      digitalWrite(PIN_lampe,HIGH);
     
}else if(message == 'o'){// lampe off
      Serial.println("lampe_eteint");
      digitalWrite(PIN_lampe,LOW);
      

}
}
